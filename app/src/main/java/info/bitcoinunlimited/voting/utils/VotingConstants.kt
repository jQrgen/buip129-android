package info.bitcoinunlimited.voting.utils

object VotingConstants {
    const val privateKeyHex = "privateKeyHex"
    const val mnemonicTableId = "mnemonic"
    const val MNEMONIC = "mnemonic"
    const val MNEMONIC_PHRASE = "mnemonic_phrase"
    const val PRIVATE_KEY = "privateKey"
}
