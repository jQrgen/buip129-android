package info.bitcoinunlimited.voting

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.widget.Toolbar
import androidx.core.os.bundleOf
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import bitcoinunlimited.libbitcoincash.ToHexStr
import com.google.android.material.navigation.NavigationView
import com.google.zxing.integration.android.IntentIntegrator
import com.google.zxing.integration.android.IntentResult
import info.bitcoinunlimited.votepeer.votePeerActivity.VotePeerActivity
import info.bitcoinunlimited.voting.utils.VotingConstants
import info.bitcoinunlimited.voting.utils.VotingConstants.PRIVATE_KEY
import info.bitcoinunlimited.voting.wallet.room.Mnemonic
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class VotingActivity : VotePeerActivity(), NavigationView.OnNavigationItemSelectedListener {
    private lateinit var drawerLayout: DrawerLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pure_voting)

        val toolbar: Toolbar? = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val privateKeyHex = getPrivateKeyHex(intent)
        initDrawerLayout(getTopLevelDestinations(), privateKeyHex)
    }

    internal fun getPrivateKeyHex(intent: Intent): String {
        val privateKey = intent.getByteArrayExtra(PRIVATE_KEY) ?: throw Exception("Cannot get privateKey from intent")
        return ToHexStr(privateKey)
    }

    private fun initDrawerLayout(votePeerLibraryTopLevelDestinations: Set<Int>, privateKeyHex: String) {
        val fragmentArgs = bundleOf(VotingConstants.privateKeyHex to privateKeyHex)
        findNavController(R.id.nav_host_fragment).setGraph(R.navigation.nav_graph_main, fragmentArgs)
        val navController = findNavController(R.id.nav_host_fragment)
        val navView: NavigationView = findViewById(R.id.nav_view)
        drawerLayout = findViewById(R.id.drawer_layout)

        val topLevelDestinations = mutableSetOf(R.id.nav_identity, R.id.nav_mnemonic, R.id.nav_login)
        topLevelDestinations.addAll(votePeerLibraryTopLevelDestinations)
        topLevelDestinations.toSet()

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            topLevelDestinations,
            drawerLayout
        )

        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        navView.setNavigationItemSelectedListener(this)

        findViewById<Toolbar>(R.id.toolbar).setNavigationOnClickListener {
            val currentDestinationId = navController.currentDestination?.id
            hideKeyBoard()

            if (topLevelDestinations.contains(currentDestinationId))
                drawerLayout.open()
            else
                navController.popBackStack()
        }

        val electionId = intent.getStringExtra("election_id") ?: return
        viewModel.handleVotePeerNotification(privateKeyHex, electionId)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

    private fun hideKeyBoard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager ?: return
        imm.hideSoftInputFromWindow(this.currentFocus?.windowToken, 0)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val result: IntentResult? = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && result != null) {
            if (result.contents == null) {
                println("result.contents == null")
            } else {
                val uri = result.contents
                viewModel.qrCodeRead(uri)
            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val sameDestinationClicked = sameDestinationClicked(item)
        if (sameDestinationClicked)
            return true
        else
            when (item.itemId) {
                R.id.menu_election_master -> {
                    navigateTo(R.id.nav_graph_election)
                }
                R.id.menu_nav_login -> {
                    navigateTo(R.id.nav_login)
                }
                R.id.menu_nav_identity -> {
                    navigateTo(R.id.nav_identity)
                }
                R.id.menu_nav_mnemonic -> {
                    val mnemonicPhrase = intent.getStringExtra(VotingConstants.MNEMONIC_PHRASE)
                        ?: throw Exception("Cannot get mnemonicPhrase from intent in VotingActivity")
                    val args = bundleOf(
                        VotingConstants.privateKeyHex to getPrivateKeyHex(intent),
                        VotingConstants.MNEMONIC to Mnemonic(mnemonicPhrase)
                    )
                    findNavController(R.id.nav_host_fragment).navigate(R.id.nav_mnemonic, args)
                    drawerLayout.closeDrawer(GravityCompat.START, true)
                }
            }
        return true
    }

    internal fun navigateTo(navId: Int): Boolean {
        val args = bundleOf(VotingConstants.privateKeyHex to getPrivateKeyHex(intent))
        findNavController(R.id.nav_host_fragment).navigate(navId, args)
        drawerLayout.closeDrawer(GravityCompat.START, true)
        return true
    }

    internal fun sameDestinationClicked(item: MenuItem): Boolean {
        val menuItemNav = arrayOf(
            Pair(R.id.nav_election_master, R.id.menu_election_master),
            Pair(R.id.nav_login, R.id.menu_nav_login),
            Pair(R.id.nav_identity, R.id.menu_nav_identity),
            Pair(R.id.nav_mnemonic, R.id.menu_nav_mnemonic)
        )
        val currentDestination = findNavController(R.id.nav_host_fragment).currentDestination?.id

        menuItemNav.forEach { destination ->
            if (destination.first == currentDestination && item.itemId == destination.second) {
                drawerLayout.closeDrawer(GravityCompat.START, true)
                return true
            }
        }
        return false
    }
}
