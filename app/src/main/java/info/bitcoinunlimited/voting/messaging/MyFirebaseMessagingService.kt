package info.bitcoinunlimited.voting.messaging

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import info.bitcoinunlimited.voting.CloudLogger
import info.bitcoinunlimited.voting.wallet.room.MnemonicDatabase
import info.bitcoinunlimited.voting.wallet.room.WalletDatabase
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class MyFirebaseMessagingService : FirebaseMessagingService() {

    private val handler = CoroutineExceptionHandler { _, exception ->
        Log.e(TAG_NOTIFICATIONS, exception.message ?: exception.toString())
        CloudLogger.recordException(exception)
    }

    override fun onNewToken(token: String) {
        Log.d(TAG_NOTIFICATIONS, "Refreshed FCM token: $token")

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // FCM registration token to your app server.
        handleNewToken(token)
    }

    private fun handleNewToken(token: String) = GlobalScope.launch(Dispatchers.IO + handler) {
        val mnemonicDatabase = MnemonicDatabase.getInstance(applicationContext)
        val mnemonic = mnemonicDatabase.getMnemonic()
        val payDestination = WalletDatabase(applicationContext, mnemonic).getPayDestination()
        MyMessagingService(applicationContext, payDestination).handleNewToken(token)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        handleMessage(remoteMessage)
    }

    private fun handleMessage(remoteMessage: RemoteMessage) = GlobalScope.launch(Dispatchers.IO + handler) {
        val mnemonicDatabase = MnemonicDatabase.getInstance(applicationContext)
        val mnemonic = mnemonicDatabase.getMnemonic()
        val walletDatabase = WalletDatabase(applicationContext, mnemonic)
        val payDestination = walletDatabase.getPayDestination()
        MyMessagingService(applicationContext, payDestination).handleRemoteMessage(remoteMessage)
    }

    companion object {
        const val TAG_NOTIFICATIONS = "MyFirebaseMsgService"
    }
}
