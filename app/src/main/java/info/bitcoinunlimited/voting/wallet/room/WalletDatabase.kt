package info.bitcoinunlimited.voting.wallet.room

import android.content.Context
import bitcoinunlimited.libbitcoincash.Bip44Wallet
import bitcoinunlimited.libbitcoincash.OpenKvpDB
import bitcoinunlimited.libbitcoincash.PayDestination
import bitcoinunlimited.libbitcoincash.PlatformContext
import info.bitcoinunlimited.votepeer.utils.Constants

class WalletDatabase(context: Context, val mnemonic: Mnemonic) {
    private val addressIndex = 0
    private val dbPrefix: String = "voter.cash"
    val wallet: Bip44Wallet

    init {
        wallet = getBip44Wallet(context, mnemonic)
    }

    fun getPayDestination(): PayDestination {
        return wallet.getDestinationAtIndex(addressIndex)
    }

    private fun getBip44Wallet(context: Context, mnemonic: Mnemonic): Bip44Wallet {
        val platFormContext = PlatformContext(context)
        val walletDb = OpenKvpDB(platFormContext, dbPrefix + "bip44walletdb")
            ?: throw Exception("Cannot init wallet! OpenKvpDB returns null in initWallet()")
        return Bip44Wallet(
            walletDb,
            "my-wallet",
            Constants.CURRENT_BLOCKCHAIN,
            mnemonic.phrase
        )
    }
}
