package info.bitcoinunlimited.voting.wallet.login

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import bitcoinunlimited.libbitcoincash.UtilStringEncoding
import info.bitcoinunlimited.votepeer.utils.InjectorUtils
import info.bitcoinunlimited.votepeer.votePeerActivity.VotePeerActivityViewModel
import info.bitcoinunlimited.voting.databinding.LoginFragmentBinding
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class LoginFragment : Fragment() {
    private lateinit var binding: LoginFragmentBinding
    private val safeArgs: LoginFragmentArgs by navArgs()

    private val votePeerActivityViewModel: VotePeerActivityViewModel by activityViewModels {
        InjectorUtils.provideVotePeerActivityViewModelFactory(
            activity?.application ?: throw IllegalStateException("Cannot get application in LoginFragment!"),
            this,
            UtilStringEncoding.hexToByteArray(safeArgs.privateKeyHex)
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = LoginFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.websiteLoginQrScanner.setOnClickListener {
            votePeerActivityViewModel.startQrScanner()
        }

        binding.loginButtonYoutubeDemo.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("https://youtu.be/yvnl5qgDK50")
            startActivity(intent)
        }
    }
}
