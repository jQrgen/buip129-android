package info.bitcoinunlimited.voting.wallet.recovery

import android.R
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.InputType.TYPE_CLASS_TEXT
import android.text.InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import info.bitcoinunlimited.voting.databinding.FragmentRecoverBinding
import info.bitcoinunlimited.voting.utils.Event
import info.bitcoinunlimited.voting.utils.InjectorUtilsApp
import info.bitcoinunlimited.voting.wallet.recovery.RecoverViewState.Recovering
import info.bitcoinunlimited.voting.wallet.recovery.RecoverViewState.Recovery
import info.bitcoinunlimited.voting.wallet.recovery.RecoverViewState.RecoveryError
import info.bitcoinunlimited.voting.wallet.recovery.RecoverViewState.RecoverySuccess
import info.bitcoinunlimited.voting.wallet.room.Mnemonic
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class RecoverFragment : Fragment(), RecoverView {
    private lateinit var binding: FragmentRecoverBinding
    private var recoveringFromMnemonicAlertDialog: AlertDialog? = null

    private val viewModel: RecoverViewModel by viewModels {
        InjectorUtilsApp.provideRecoverViewModelFactory(
            activity?.application ?: throw Exception("Cannot get application in RecoverFragmeent"),
        )
    }
    private val intent = RecoverViewIntent()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRecoverBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.bindIntents(this)
        initClickListeners()
        binding.oldMnemonicInput.inputType = TYPE_CLASS_TEXT or TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
    }

    private fun initClickListeners() {
        binding.replaceMnemonicButton.setOnClickListener {
            val mnemonicPhrase = binding.oldMnemonicInput.text.toString()
            intent.recoverMnemonic.value = Event(
                RecoverViewIntent.RecoverMnemonic(
                    Mnemonic(mnemonicPhrase),
                    activity?.applicationContext
                        ?: throw Exception("Cannot find applicationContext in RecoverFragment")
                )
            )
        }
    }

    override fun initState() = intent.initState

    override fun recoverFromMnemonic() = intent.recoverMnemonic

    override fun render(state: RecoverViewState) {
        when (state) {
            is Recovery -> println("Default state")
            is Recovering -> renderRecovering(state)
            is RecoverySuccess -> renderRecoverySuccess()
            is RecoveryError -> renderRecoveryError(state)
        }

        if (state is Recovery || state is RecoverySuccess || state is RecoveryError)
            renderLoading(false)
        else if (state is Recovering)
            renderLoading(true)

        if (state is RecoverySuccess || state is RecoveryError)
            recoveringFromMnemonicAlertDialog?.hide()
    }

    private fun renderRecovering(state: Recovering) {
        recoveringFromMnemonicAlertDialog = MaterialAlertDialogBuilder(this.requireContext())
            .setTitle("Recovering Mnemonic...")
            .setMessage(state.message)
            .setNeutralButton("ok") { _, _ ->
                // Do nothing.
            }
            .show()
    }

    @SuppressLint("ObsoleteSdkInt")
    private fun resetApplication() {
        requireActivity().finish()
        System.exit(0)
        // exitProcess(0)
        val resetApplicationIntent = requireActivity().packageManager.getLaunchIntentForPackage(requireActivity().packageName)
        if (resetApplicationIntent != null) {
            resetApplicationIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        startActivity(resetApplicationIntent)
        (context as Activity).overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    private fun renderRecoverySuccess() {
        MaterialAlertDialogBuilder(this.requireContext())
            .setTitle("Recovery success. The application will now close.")
            .setMessage("Open the application to use the identity from the recovered mnemonic")
            .setNeutralButton("Understood") { _, _ ->
                // Do nothing.
            }
            .show()
            .setOnDismissListener {
                Toast.makeText(
                    activity?.applicationContext,
                    "VotePeer: Success. Re-open application to use the new key", Toast.LENGTH_LONG
                ).show()
                recoveringFromMnemonicAlertDialog?.cancel()
                resetApplication()
            }
    }

    internal fun renderRecoveryError(recoveryError: RecoveryError) {
        MaterialAlertDialogBuilder(this.requireContext())
            .setTitle("Error")
            .setMessage(recoveryError.message)
            .setNeutralButton("Got it") { _, _ ->
                // Do nothing.
            }
            .show()
    }

    private fun renderLoading(isLoading: Boolean) {
        if (isLoading) {
            binding.replaceMnemonicButton.visibility = View.GONE
            binding.recoveringMnemonicProgressBar.visibility = View.VISIBLE
        } else {
            binding.replaceMnemonicButton.visibility = View.VISIBLE
            binding.recoveringMnemonicProgressBar.visibility = View.GONE
        }
    }
}
