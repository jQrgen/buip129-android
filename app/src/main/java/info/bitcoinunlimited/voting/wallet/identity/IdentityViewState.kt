package info.bitcoinunlimited.voting.wallet.identity

import android.graphics.Bitmap

sealed class IdentityViewState {
    data class BalanceProgressbar(
        val enabled: Boolean = false
    ) : IdentityViewState()

    data class Balance(
        val balance: Int?
    ) : IdentityViewState()

    data class Identity(
        val address: String,
        val qrCode: Bitmap
    ) : IdentityViewState()

    data class Problem(
        val exception: Throwable
    ) : IdentityViewState()
}
