package info.bitcoinunlimited.voting.wallet.room

import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import bitcoinunlimited.libbitcoincash.GenerateBip39SecretWords
import bitcoinunlimited.libbitcoincash.GenerateEntropy
import info.bitcoinunlimited.voting.utils.TAG_WALLET_REPOSITORY
import info.bitcoinunlimited.voting.utils.VotingConstants

@ExperimentalUnsignedTypes
@Database(entities = [Mnemonic::class], version = 4, exportSchema = false)
abstract class MnemonicDatabase : RoomDatabase() {
    abstract fun mnemonicDao(): MnemonicDao

    suspend fun getMnemonic(): Mnemonic {
        val mnemonicLocalStorage = mnemonicDao().getMnemonic(VotingConstants.mnemonicTableId)
        return if (mnemonicLocalStorage == null) {
            Log.i(TAG_WALLET_REPOSITORY, "Mnemonic not found. Generating new mnemonic.")
            val entropy = GenerateEntropy(128)
            val words = GenerateBip39SecretWords(entropy)
            val mnemonic = Mnemonic(words)

            mnemonicDao().insert(mnemonic)
            mnemonic
        } else {
            mnemonicLocalStorage
        }
    }

    suspend fun setMnemonic(mnemonic: Mnemonic) {
        mnemonicDao().insert(mnemonic)
    }

    companion object {
        // For Singleton instantiation
        @Volatile private var instance: MnemonicDatabase? = null

        fun getInstance(context: Context): MnemonicDatabase {
            return instance
                ?: synchronized(this) {
                    instance
                        ?: buildDatabase(
                            context
                        )
                            .also { instance = it }
                }
        }

        private fun buildDatabase(context: Context): MnemonicDatabase {
            return Room.databaseBuilder(
                context, MnemonicDatabase::class.java,
                "mnemonic-db"
            ).fallbackToDestructiveMigration().build()
        }
    }
}
