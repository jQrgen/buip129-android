package info.bitcoinunlimited.voting.wallet.recovery

import info.bitcoinunlimited.voting.utils.Event
import info.bitcoinunlimited.voting.wallet.recovery.RecoverViewIntent.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow

@ExperimentalCoroutinesApi
interface RecoverView {
    /**
     * Intent to load the current Mnemonic state
     *
     * @return A flow that inits the current MnemonicViewState
     */
    fun initState(): Flow<Event<Boolean>>

    /**
     * Intent to submit the mnemonic
     *
     * @return A flow that emits the mnemonic
     */
    fun recoverFromMnemonic(): Flow<Event<RecoverMnemonic?>>

    /**
     * Renders the MnemonicViewState
     *
     * @param state The current view state display
     */
    fun render(state: RecoverViewState)
}
