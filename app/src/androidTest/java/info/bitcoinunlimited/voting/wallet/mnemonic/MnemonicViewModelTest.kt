package info.bitcoinunlimited.voting.wallet.mnemonic

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import bitcoinunlimited.libbitcoincash.ToHexStr
import info.bitcoinunlimited.voting.VotePeerMock
import info.bitcoinunlimited.voting.wallet.MnemonicMock
import info.bitcoinunlimited.voting.wallet.room.MnemonicDatabase
import info.bitcoinunlimited.voting.wallet.room.WalletDatabase
import io.mockk.coVerify
import io.mockk.mockk
import io.mockk.spyk
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@InternalCoroutinesApi
@ExperimentalCoroutinesApi
internal class MnemonicViewModelTest {
    private lateinit var viewModel: MnemonicViewModel
    private val mnemonicMock = MnemonicMock.getMnemonic()
    private val mnemonicViewStateMock = MnemonicViewState.MnemonicAvailable(mnemonicMock)

    @BeforeEach
    fun setUp() = runBlocking {
        val applicationContext = ApplicationProvider.getApplicationContext() as Context
        val mnemonicDao = MnemonicDatabase.getInstance(applicationContext).mnemonicDao()
        val mnemonic = MnemonicDatabase.getInstance(applicationContext).getMnemonic()
        val walletDb = WalletDatabase(applicationContext, mnemonic)
        val privateKeyMock = VotePeerMock.mockPrivateKey()
        val privateKeyHexMock = ToHexStr(privateKeyMock)

        viewModel = spyk(MnemonicViewModel(privateKeyHexMock, mnemonic))
    }

    @AfterEach
    fun tearDown() {
        viewModel.state.value = null
    }

    @Test
    fun bindIntents() {
        val mnemonicFragment = mockk<MnemonicFragment>(relaxed = true)
        viewModel.state.value = mnemonicViewStateMock
        viewModel.bindIntents(mnemonicFragment)
        coVerify { viewModel.state.filterNotNull() }
        assertEquals(mnemonicViewStateMock, viewModel.state.value)
    }

    @Test
    fun submitMnemonic() {
        viewModel.submitMnemonic(MnemonicViewIntent.SubmitMnemonic(mnemonicMock.phrase))
        assertEquals(mnemonicViewStateMock, viewModel.state.value)
    }

    @Test
    fun submitEmptyMnemonic() = runBlockingTest {
        val mnemonicMockEmpty = ""
        viewModel.submitMnemonic(MnemonicViewIntent.SubmitMnemonic(mnemonicMockEmpty))
        val mnemonicSubmitErrorMock = MnemonicViewState.MnemonicErrorMessage("Mnemonic is empty!")

        assertEquals(mnemonicSubmitErrorMock, viewModel.state.value)
    }
}
