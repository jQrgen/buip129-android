package info.bitcoinunlimited.voting.wallet

import info.bitcoinunlimited.voting.wallet.room.Mnemonic

object MnemonicMock {
    fun getMnemonic(): Mnemonic {
        return Mnemonic("cupboard two beach document doctor toward behave attend cliff mesh dove stool")
    }
}
