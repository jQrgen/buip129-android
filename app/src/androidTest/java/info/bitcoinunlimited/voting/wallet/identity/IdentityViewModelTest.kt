package info.bitcoinunlimited.voting.wallet.identity

import android.graphics.Bitmap
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import bitcoinunlimited.libbitcoincash.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.voting.VotePeerMock
import io.mockk.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers.instanceOf
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class IdentityViewModelTest {
    private val qrHeight = 512
    private val qrWidth = 512
    private val addressMock = "mock address"
    private val privateKey = VotePeerMock.mockPrivateKey('A')
    private lateinit var viewModel: IdentityViewModel
    private lateinit var payAddress: PayAddress
    private lateinit var electrumAPI: ElectrumAPI

    @BeforeEach
    fun setUp() {
        electrumAPI = spyk(ElectrumAPI.getInstance(ChainSelector.BCHMAINNET))
        payAddress = VotePeerMock.mockP2pkhAddress(ChainSelector.BCHTESTNET, privateKey)
        viewModel = spyk(IdentityViewModel(electrumAPI, payAddress))
    }

    @Test
    fun bindIntents() = runBlockingTest {
        val identityFragment = mockk<IdentityView>(relaxed = true)
        val mockBalance = ElectrumClient.BalanceResult(60, 40)
        val viewState = IdentityViewState.Balance(100)
        coEvery { electrumAPI.getBalance(any()) } returns mockBalance
        viewModel.state.value = viewState

        viewModel.bindIntents(identityFragment)
        coVerify { identityFragment.initState() }
        coVerify { identityFragment.refreshBalanceClicked() }
        coVerify { viewModel.state.filterNotNull() }
        assertThat(viewModel.state.value, instanceOf(IdentityViewState.Balance::class.java))
    }

    @Test
    fun fetchBalanceTest() = runBlockingTest {
        val balanceResult = ElectrumClient.BalanceResult(1300, 37)
        coEvery { electrumAPI.getBalance(payAddress) } returns balanceResult
        viewModel.fetchBalance()
        coVerify { viewModel.setState(IdentityViewState.Balance(1337)) }
    }

    @Test
    fun identityViewModelExceptionHandler() = runBlockingTest {
        val error = Exception("Error in IdentityViewModel")
        val identityError = IdentityViewState.Problem(error)
        viewModel.handler.handleException(CoroutineScope(Dispatchers.Main).coroutineContext, error)
        assertEquals(identityError.exception.message, error.message)
    }

    @Test fun generateQrCodeTest() = runBlocking {
        val qrHeight = 512
        val qrWidth = 512
        val qrCode = viewModel.generateQrCode(addressMock, qrHeight, qrWidth)
        assertEquals(qrHeight, qrCode.height)
        assertEquals(qrWidth, qrCode.width)
        assertEquals(Bitmap.Config.RGB_565, qrCode.config)
    }

    @Test fun generateQrCodeEmptyInputTest() = runBlockingTest {
        val qrHeight = 512
        val qrWidth = 512
        val exception = assertThrows<IllegalArgumentException> {
            viewModel.generateQrCode("", qrHeight, qrWidth)
        }
        assertEquals(exception.message, IdentityViewModel.addressEmpty)
    }
}
