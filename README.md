# Voter.cash Android voting application

<!-- For developers: https://about.gitlab.com/handbook/markdown-guide/> -->

[ico-telegram]: app/telegram.svg
[link-telegram]: https://t.me/buip129
[![Chat on Telegram][ico-telegram]][link-telegram]
[![Twitter URL](https://img.shields.io/twitter/url/https/twitter.com/votepeer.svg?style=social&label=Follow%20%40votepeer)](https://twitter.com/votepeer)
[![MIT license](https://img.shields.io/github/license/Naereen/StrapDown.js.svg)](LICENSE)
[![Website](https://img.shields.io/badge/Website-Online-green.svg)](https://voter.cash/)

<span style="left:1000px">
  <a align="center" href="https://gitlab.com/nerdekollektivet/votepeer-android-app/-/jobs/artifacts/master/raw/app-release.apk?job=assemble_release">
  <img src="apk-download-badge.png" width="27%">
  </a>

  <a align="center" href="https://play.google.com/store/apps/details?id=info.bitcoinunlimited.voting">
  <img src="https://cdn.rawgit.com/steverichey/google-play-badge-svg/master/img/en_get.svg" width="27%">
  </a>
</span>

### Clone project
`git clone git@gitlab.com:nerdekollektivet/buip129-android.git`

#### Optionally clone with INTELLIJ/Android studio
file -> Project form version control -> `git@gitlab.com:nerdekollektivet/buip129-android.git`

### INTELLIJ/Android studio: Project SDK
Make sure Project SDk is added user project:

`File-> Project structure -> Project settings -> project`

-> Make sure the latest Android SDK is selected under Project SDK


### Setup Android sdk path in local.properties (mac path)
`touch local.properties`
```

local.properties:
```properties
## This file must *NOT* be checked into Version Control Systems,
# as it contains information specific to your local configuration.
#
# Location of the SDK. This is only used by Gradle.
# For customization when using a Version Control System, please read the
# header note.
#Fri Apr 24 17:09:51 CEST 2020
#ndk.dir=/Users/jqrgen/Library/Android/sdk/ndk/21.0.6113669
sdk.dir=/Users/<user>/Library/Android/sdk 
```

file->invalidate cashes and restart


### Setup with firebase
Follow this tutorial to setup a firebase project and retrieve a google-services.json file

https://firebase.google.com/docs/android/setup

add google-service.json file to: `app/google-services.json`


## Intellij IDEA

To make Intellij IDEA's built-in formatter produce 100% ktlint-compatible code, run once

`./gradlew ktlintApplyToIDEAProject`

## Android Instrumented testing

```kotlin
./gradlew connectedAndroidTest
```

Android instrumented test required the votepeer android library to be imported locally.

It also requires an android device or emulator

app-level build.gradle:

```kotlin
    // NOTE: This dependency is required for connected tests
    // TODO: Decouple local implementation from instrumented test dependencies
    implementation project(":votepeer")
    // implementation "info.bitcoinunlimited:votepeer:3.0.0"
```

settings.gradle:

Add the relative path to where you have downloaded the votepeer library here

```kotlin
    project(':votepeer').projectDir = new File(settingsDir, '../votepeer-library/votepeer')
```

Votepeer library: https://gitlab.com/nerdekollektivvotepeer-library
Android developer documentation: https://developer.android.com/studio/test/command-line#RunTestsGradle

### Android Instrumented test results
These tests currently requires an emulator, or a device to run. Thus they cannot be displayed in CI/CD

<img src="https://i.imgur.com/AQVKjkQ.png">

## Jacoco: Code coverage reporting

##### Coverage reports for the unit tests
```shell
./gradlew jacocoTestDebugUnitTestReport

App => {rootDir}/app/build/jacoco/jacoco.xml //unit tests
Library => ${rootDir}../VotePeerLibrary/votepeer/build/jacoco/jacoco.xml //unit tests
```


##### Coverage reports for the UI tests
`See: https://vsahin.com/2021/06/21/coverage-reports-in-android-and-sonarqube/`
```shell
./gradlew createDebugCoverageReport

App => "${rootDir}/app/build/reports/coverage/debug/report.xml", //ui tests
Library => ${rootDir}../VotePeerLibrary/votepeer/build/reports/coverage/debug/report.xml //ui tests
```

## Permanent link to latest release apk:

https://gitlab.com/nerdekollektivet/votepeer-android-app/-/jobs/artifacts/master/raw/app-release.apk?job=assemble-release

ref: https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#how-searching-for-job-artifacts-works
