# Contributing guidelines

## Forking
Create a personal fork of this repository for your work

Sign into Gitlab under your account, then visit the project at git@gitlab.com:nerdekollektivet/buip129-android.git

Click the 'Fork' button on the top right, and choose to fork the project to your personal Gitlab space.

Clone your personal work repository to your local machine:

git clone git@gitlab.com:nerdekollektivet/buip129-android.git

Set your checked out copy's upstream to our main project:

git remote add upstream git@gitlab.com:nerdekollektivet/buip129-android.git


## Workflow

A typical workflow would be:

Create a topic branch in Git for your changes
git checkout -b 'my-topic-branch'

Make your changes, and commit them
git commit -a -m 'my-commit'

Push the topic branch to your Gitlab repository
git push -u origin my-topic-branch

Then create a Merge Request (the Gitlab equivalent of a Pull Request)
from that branch in your personal repository. To do this, you need to
sign in to Gitlab, go to the branch and click the button which lets you
create a Merge Request (you need to fill out at least title and description
fields).

Work with us on Gitlab to receive review comments, going back to the
'Make your changes' step above if needed to make further changes on your
branch, and push them upstream as above. They will automatically appear
in your Merge Request.

All Merge Requests should contain a commit with a test plan that details
how to test the changes. In all normal circumstances, you should build and
test your changes locally before creating a Merge Request.
A merge request should feature a specific test plan where possible, and
indicate which regression tests may need to be run.
If you have doubts about the scope of testing needed for your change,
please contact our developers and they will help you decide.

For large changes, break them into several Merge Requests.

If you are making numerous changes and rebuilding often, it's highly
recommended to install ccache (re-run cmake if you install it
later), as this will help cut your re-build times from several minutes to under
a minute, in many cases.